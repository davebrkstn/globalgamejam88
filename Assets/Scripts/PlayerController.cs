﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Spine;
using Spine.Unity;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rigidBody;

    //the highest a jump can ever be
    public float maxJumpForce = 10;
    
    //If the player jumps X in a row, the force will get smaller
    private float currentJumpForce;
    
    //Horizontal Movement
    public float horizontalSpeed = 10;
    
    //Set by Input Manager, either -1,0,1
    private float horizontalAxis;

    //Set to True if player touches ground collider
    private bool isGrounded;

    //Most jumps in a row that can be done
    public int maxJumps = 2;

    //Current jump count
    private int currentJump = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        var skeletonAnimation = GetComponent<SkeletonAnimation>();

        skeletonAnimation.AnimationState.SetAnimation(0, "Idle", true);

        rigidBody = GetComponent<Rigidbody2D>();

        currentJumpForce = maxJumpForce;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        
        if (Input.GetKeyDown(KeyCode.K))
        {
            MountShell();
        }
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
    }

    private void Move()
    {
        //Get horizontal axis and apply movement to charater
        horizontalAxis = (Input.GetAxisRaw("Horizontal"));
        rigidBody.AddForce(new Vector2(horizontalAxis * horizontalSpeed, 0), ForceMode2D.Force);

        //Spine animation change
        //if (horizontalAxis == 0)
        // {
        //    var skeletonAnimation = GetComponent<SkeletonAnimation>();
        //    skeletonAnimation.AnimationState.SetAnimation(0, "Idle", true);
        // }
        // else 
        //  {
        //    var skeletonAnimation = GetComponent<SkeletonAnimation>();
        //    skeletonAnimation.AnimationState.SetAnimation(0, "Run", true);
         // }
    }

    private void Jump()
    {
        if (currentJump <= maxJumps)
        {
            rigidBody.AddForce(new Vector2(0,currentJumpForce),ForceMode2D.Impulse);
            currentJump++;
            currentJumpForce *= 0.5f; //Makes the jumps smaller each time
        }  
    }

    private void MountShell()
    {
        //TODO
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals("Shell"))
        {
            Debug.Log("Hit Shell");
        }
        
        //Simpel grounding check.
        if (other.gameObject.tag.Equals("Ground"))
        {
            ResetGrounding();
        }   
    }

    void ResetGrounding()
    {
        isGrounded = true;

        currentJumpForce = maxJumpForce;

        currentJump = 0;
    }
}

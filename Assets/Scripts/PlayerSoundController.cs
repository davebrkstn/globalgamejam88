﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundController : MonoBehaviour
{

    public AudioSource jumpSound;

    public AudioSource runSound;
    // Start is called before the first frame update

    public AudioSource removeShellSound;
    
    public AudioSource hideInShellSound;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayHideInShellSound()
    {
        if (hideInShellSound != null)
        {
            hideInShellSound.Play();
        }
    }

    public void PlayRemoveShellSound()
    {
        if (removeShellSound != null)
        {
            removeShellSound.Play();
        }
    }

    public void PlayJumpSound()
    {
        if (jumpSound != null)
        {
            jumpSound.Play();
        }
    }

    public void PlayRunSound()
    {
        if(runSound != null)
        {
            if (!runSound.isPlaying)
            {
                runSound.Play();
            }
        }
    }

    public void StopRunSound()
    {
        if (runSound != null)
        {
            runSound.Stop();
        }
    }
    
}

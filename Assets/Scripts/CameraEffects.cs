﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class CameraEffects : MonoBehaviour
{
    public PostProcessVolume postProcessing;

    public LensDistortion distortion;

    public float intensityLow;
    public float intensityHigh;

    public float intencityRate;

    public float startingDirection;
    // Start is called before the first frame update
    void Start()
    {
        postProcessing.profile.TryGetSettings(out distortion);
        
        startingDirection = 1;
        intensityLow =  -40;
        intensityHigh = 40;

        intencityRate = 10f;

    }

    // Update is called once per frame
    void Update()
    {
        
        float ra = distortion.intensity;

        if (ra < intensityLow || ra > intensityHigh)
        {
            startingDirection *= -1;
        }

        ra += (intencityRate * startingDirection) * Time.deltaTime;

        distortion.intensity.value = ra;
    }
}

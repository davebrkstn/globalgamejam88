﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwimmingFish : MonoBehaviour
{
    public float speed;
    private float direction; 
    
    public float travelDistance = 5; 
    public float leftLimmit = -10;

    public float rightLimmit = 10;
    // Start is called before the first frame update
    void Start()
    {
        direction = -1;
        leftLimmit = transform.position.x - travelDistance;
        rightLimmit = transform.position.x + travelDistance;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x < leftLimmit)
        {
            direction = 1;
            transform.Rotate(0,180,0);
        }

        if (transform.position.x > rightLimmit)
        {
            direction = -1;
            
            transform.Rotate(0,180,0);
        }
        
        Vector3 pos = transform.position;
        pos.x += speed * direction * Time.deltaTime;
        transform.position = pos;

        
    }
}

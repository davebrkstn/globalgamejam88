﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

public class PlayerMovement : MonoBehaviour
{
	public CharacterController2D controller;
	
	public float runSpeed  = 40f;
	float horizontalMove = 0f;
	float verticalMove = 0f;
	bool jump = false;
	bool hidden = false;
	public GameObject attachedShell;

	private string shellName;
	
	private int jumpCount = 0;

	public PlayerSoundController soundCOntroller;
	
    // Update is called once per frame
    void Update()
    { 	
		if(!(this.GetComponent<AttachShell>().GetDead())){
			horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
			if (horizontalMove != 0 & controller.isGrounded)
			{
				soundCOntroller.PlayRunSound();
			}
			else
			{
				soundCOntroller.StopRunSound();
			}
			
			
			verticalMove = Input.GetAxisRaw("Vertical");
			if (Input.GetButtonDown("Jump"))
			{
				jump = true;
				soundCOntroller.PlayJumpSound();
				
				TriggerAnimation("Jump");
				
			}
			else
			{
				//spine annimation check and set Idle if not jumping or moving
				if (horizontalMove == 0)
				{
					TriggerAnimation("Idle");                
				}
				//spine annimation check and set Idle if not jumping and currently moving
				else
				{
					if(!hidden)
						TriggerAnimation("Run"); 
				}
			}
		}
    }

	public void TriggerAnimation(string animationScene){
		var skeletonAnimation = GetComponent<SkeletonAnimation>();
		if (skeletonAnimation != null)
		{
			if (animationScene == "Idle"){
				Spine.Animation anim1 = skeletonAnimation.skeleton.Data.FindAnimation("Death");
				Spine.Animation anim2 = skeletonAnimation.skeleton.Data.FindAnimation("Hide");
				bool isRun1 = (skeletonAnimation.state.GetCurrent(0).Animation == anim1);	
				bool isRun2 = (skeletonAnimation.state.GetCurrent(0).Animation == anim2);	
				
				if ( !(isRun1) && !(isRun2))
				 {
				    Spine.Animation anim = skeletonAnimation.skeleton.Data.FindAnimation(animationScene);
				    bool isRun = (skeletonAnimation.state.GetCurrent(0).Animation == anim);			
				    if (!isRun)
					{
						skeletonAnimation.AnimationState.SetAnimation(0, animationScene, true);
					}
				 }
				
			}else{
				if ((animationScene == "Death") || (animationScene == "Hide"))
				 {
					Spine.Animation anim = skeletonAnimation.skeleton.Data.FindAnimation(animationScene);
			  	    bool isRun = (skeletonAnimation.state.GetCurrent(0).Animation == anim);			
				    if (!isRun)
			         {
				 	  skeletonAnimation.AnimationState.SetAnimation(0, animationScene, false);
				     } 
				 }else{

					Spine.Animation anim = skeletonAnimation.skeleton.Data.FindAnimation(animationScene);
					bool isRun = (skeletonAnimation.state.GetCurrent(0).Animation == anim);			
					if (!isRun)
				 	 {
					  skeletonAnimation.AnimationState.SetAnimation(0, animationScene, true);
				     }
				}
		}
		}
	}
		
	void FixedUpdate(){
		if(!hidden){
			//Move our character
			controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
			jump = false;
		}
		bool isShellAttached = this.GetComponent<AttachShell>().IsShellAttached();
		if(isShellAttached){
			if(verticalMove < 0f){
				//HideInShell();
				TriggerAnimation("Hide");
				soundCOntroller.PlayHideInShellSound();
				hidden = true;
			}
			if(hidden && (verticalMove == 0f)){
				//ComeOutOfShell();
				TriggerAnimation("Run");
				hidden = false;
			}
		}
	}

	private void HideInShell(){
		Quaternion target =Quaternion.Euler(0, 0, 0);
		HidePositions();		
		if(this.transform.localScale.x > 0){	
			//attachedShell.transform.localPosition = new Vector3(-1.95f,2.13f,0f);
			target = Quaternion.Euler(0, 0, -45f);
		}else if(this.transform.localScale.x < 0){
			target = Quaternion.Euler(0, 0, 45f);
		}
		attachedShell.transform.rotation = target;
		hidden = true;
	}

	private void ComeOutOfShell(){
		attachedShell.transform.localPosition = new Vector3(-4f,3.53f,0f);
		Quaternion target = Quaternion.Euler(0, 0, 0);
		attachedShell.transform.rotation = target;
		hidden = false;
	}

	public bool GetHidden(){
		return hidden;
	}

	public void SetHidden(bool hide){
		hidden = hide;
	}

	public void SetShellName(string name){
		shellName = name;
	}

	private void HidePositions(){
		switch(shellName){
            case "Shell":
                attachedShell.transform.localPosition = new Vector3(-1.95f,2.13f,0f);
                break;
            case "Boot":                
                attachedShell.transform.localPosition = new Vector3(-3.52f,7.54f,0f);
                break;
        }
	}

	private void ComeOutPositions(){
		switch(shellName){
            case "Shell":
                attachedShell.transform.localPosition = new Vector3(-4f,3.53f,0f);
                break;
            case "Boot":                
                attachedShell.transform.localPosition = new Vector3(-8.85f,6.14f,0f);
                break;
        }
	}
}

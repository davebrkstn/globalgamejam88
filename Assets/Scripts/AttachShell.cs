﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Spine;
using Spine.Unity;

public class AttachShell : MonoBehaviour
{
    
	bool touchingShell = false;
    bool isAttached = false;
	public GameObject attchedShell;
	private GameObject unAttchedShell;   
	public GameObject tempShellSprite;
    public PlayerMovement player;
    public Shell shell;
    private string shellName;
    public float targetTime = 3.0f;
    private bool dead = false;

    public PlayerSoundController soundController;
	
    // Update is called once per frame
    void Update()
    {
        if(dead){
            targetTime -= Time.deltaTime; 
            if (targetTime <= 0.0f)
            {
                SceneManager.LoadScene("MainMenu");
              
               
            }
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
          	MountShell();
          	touchingShell = false;
        }
    }
	
    public bool GetDead(){
        return dead;
    }

	private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals("Shell"))
        {
			touchingShell = true;
            unAttchedShell = other.gameObject;
        }  

        if (other.gameObject.tag.Equals("Enemy"))
        {
            Debug.Log("Hit");
            EnemyHit();
        } 
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        //when we stop touching the shell set touchingShell = false
        if (other.gameObject.tag.Equals("Shell"))
        {
			touchingShell = false;
        }  
    }
	
	private void MountShell()
    {
        //If shell is NOT attached
        if(!isAttached){
            //Can only mount when touching
            if(touchingShell && attchedShell != null & unAttchedShell !=null){
                
                //Possible replace

                //attchedShell.GetComponent<SpriteRenderer>().sprite = unAttchedShell.GetComponent<SpriteRenderer>().sprite;
                tempShellSprite.GetComponent<SpriteRenderer>().sprite = unAttchedShell.GetComponent<SpriteRenderer>().sprite;
                //attchedShell.GetComponent<SpriteRenderer>().flipX = true;
                

                //var skeletonAnimation = GetComponent<SkeletonAnimation>();
                //skeletonAnimation.skeleton.SetAttachment("Shells-1","Shells-1");
                

                //Set the health of the newly attached Shell = the health of the the touched shell
                int unAttachedShellHealth = unAttchedShell.GetComponent<Shell>().GetShellHealth();
                //Debug.Log(unAttachedShellHealth);
                attchedShell.GetComponent<Shell>().SetShellHealth(unAttachedShellHealth);
                shellName = unAttchedShell.transform.name;
                this.GetComponent<PlayerMovement>().SetShellName(shellName);
                AttachSpineShell();
                //Destroy unAttached Shell
                Destroy(unAttchedShell);

                //AdjustShellMount();
                //attchedShell.transform.localScale = new Vector3(2f,2f,2f);
                isAttached = true;
                
                soundController.PlayHideInShellSound();
            }
        }else{
            CreateNewShell();
            this.GetComponent<PlayerMovement>().SetHidden(false);
        }
    }	

    private void CreateNewShell(){
        //hides the animation shell
        soundController.PlayRemoveShellSound();
        DetachSpineShell();

        attchedShell.GetComponent<SpriteRenderer>().sprite = tempShellSprite.GetComponent<SpriteRenderer>().sprite;

        GameObject droppedShell = Instantiate(attchedShell, attchedShell.transform.position, attchedShell.transform.rotation);
        Rigidbody2D droppedShellsRigidBody = droppedShell.AddComponent<Rigidbody2D>(); // Add the rigidbody.
        BoxCollider2D bc = (BoxCollider2D)droppedShell.AddComponent(typeof(BoxCollider2D));
        bc.offset = Vector3.zero;
        droppedShellsRigidBody.mass = 2;  
        droppedShell.tag = "Shell"; 
        droppedShell.transform.name = shellName; 

        
        droppedShell.transform.localScale = new Vector3(1f,1f,1f);
        
        attchedShell.GetComponent<SpriteRenderer>().sprite = null;
        isAttached = false;
    }

    private void EnemyHit(){
        //if player has a Shell attached
         if(isAttached){
            int health = attchedShell.GetComponent<Shell>().GetShellHealth();
            if(health == 1){
                isAttached = false;
            }
            shell.ShellHit();
            //if we are hidden
            if(player.GetHidden()){
               // Debug.Log("Hidden");
            }else{
                //Dont create a new shell if shell if destroyed
                if(health > 0){
                    CreateNewShell();                
                    //add Force        
                }        
            }
         }else{
            this.GetComponent<PlayerMovement>().TriggerAnimation("Death");
            dead = true;
         }
    }

    private void AdjustShellMount(){
        switch(shellName){
            case "Shell":
                attchedShell.transform.localPosition = new Vector3(-4f,3.53f,0f);
                break;
            case "Boot":                
                attchedShell.transform.localPosition = new Vector3(-8.85f,6.14f,0f);
                break;
        }
    }

    private void AttachSpineShell(){        
        var skeletonAnimation = GetComponent<SkeletonAnimation>();
        if (shellName.Contains("Shell"))
        {
            skeletonAnimation.skeleton.SetAttachment("Shells-1", "Shells-1");
        }
        else if (shellName.Contains("Boot"))
        {
            skeletonAnimation.skeleton.SetAttachment("bootPart1", "bootPart1");
            skeletonAnimation.skeleton.SetAttachment("bootPart2", "bootPart2");
            skeletonAnimation.skeleton.SetAttachment("bootPart3", "bootPart3");
        }
        else if (shellName.Contains("Can"))
        {
            skeletonAnimation.skeleton.SetAttachment("Can-Shell", "Can-Shell");
            skeletonAnimation.skeleton.SetAttachment("Can-Shell-back", "Can-Shell-back");
        }

        else if (shellName.Contains("Coral"))
        {
            skeletonAnimation.skeleton.SetAttachment("Coral-shell", "Coral-shell");
            skeletonAnimation.skeleton.SetAttachment("Coral-shell", "Coral-shell");
        }
    }

    private void DetachSpineShell(){        
        var skeletonAnimation = GetComponent<SkeletonAnimation>();
        if (shellName.Contains("Shell"))
        {
            skeletonAnimation.skeleton.SetAttachment("Shells-1", null);
        }
        else if (shellName.Contains("Boot")) { 
            skeletonAnimation.skeleton.SetAttachment("bootPart1", null);
            skeletonAnimation.skeleton.SetAttachment("bootPart2", null);
            skeletonAnimation.skeleton.SetAttachment("bootPart3", null);
        }
        else if (shellName.Contains("Can"))
        {        
                skeletonAnimation.skeleton.SetAttachment("Can-Shell",null);
                skeletonAnimation.skeleton.SetAttachment("Can-Shell-back",null);
        }

        else if (shellName.Contains("CoralShell"))
        {
            skeletonAnimation.skeleton.SetAttachment("Coral-shell", null);
            skeletonAnimation.skeleton.SetAttachment("Coral-shell", null);
        }

    }

    public bool IsShellAttached(){
        return isAttached;
    }	
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public AudioSource mainMusic;
    
    private bool isPaused;

    public GameObject pauseMenu;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPaused)
            {
                PauseGame();
            }
            else
            {
                {
                    UnpauseGame();
                }
            }
        }
    }

    public void PauseGame()
    {
        if (pauseMenu != null)
        {
            pauseMenu.SetActive(true);
        }
        Time.timeScale = 0;
        isPaused = true;
    }

    public void UnpauseGame()
    {
        if (pauseMenu != null)
        {
            pauseMenu.SetActive(false);
        }
        Time.timeScale = 1;
        isPaused = false;
    }

    public void MuteMusic()
    {
        if (mainMusic != null)
        {
            if (mainMusic.isPlaying)
            {
                mainMusic.Stop();
            }
            else
            {
                {
                    mainMusic.Play();
                }
            }
        }
    }

    public void ReturnToMenu()
    {
        UnpauseGame();

        SceneManager.LoadScene("MainMenu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}

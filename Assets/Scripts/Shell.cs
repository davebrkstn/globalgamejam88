﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour
{
    public int shellHealth;    

    public void ShellHit(){
        shellHealth--;
        if(shellHealth > 0){
            //Add Crack and hit/particle effect
            //Add Sound
        }else{
            DestroyShell();
        }
    }

    private void DestroyShell(){
        this.GetComponent<SpriteRenderer>().sprite = null;
        //add destroy effect
    }

    public void SetShellHealth(int health){
        shellHealth = health;
    }

    public int GetShellHealth(){
        return shellHealth;
    }
}

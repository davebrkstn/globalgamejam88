﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourShift : MonoBehaviour
{

    public ColourBlinker[] layers;
    // Start is called before the first frame update
    void Awake()
    {
        layers = FindObjectsOfType<ColourBlinker>();
    }

    // Update is called once per frame
    void Update()
    {
        if (layers.Length < 5)
        {
            layers = FindObjectsOfType<ColourBlinker>();
        }
    }

    public void TriggerLevelTwo()
    {
        if (layers != null)
        {
            foreach (var layer in layers)
            {
                layer.SetLevelTwo();
            }
        }
    }
    
    public void TriggerLevelThree()
    {
        if (layers != null)
        {
            foreach (var layer in layers)
            {
                layer.SetLevelThree();
            }
        }
    }

    public void TriggerEndGame()
    {
        if (layers != null)
        {
            foreach (var layer in layers)
            {
                layer.FadeToBlack();
            }
        }
    }
}

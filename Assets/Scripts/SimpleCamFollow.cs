﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCamFollow : MonoBehaviour
{
    public float interpVelocity;
    public float minDistance;
    public float followDistance;
    public GameObject target;
    public Vector3 offset;
    Vector3 targetPos;

    private bool isAtEndGame;

    private float maxEndGameCameraSize = 40;

    public float zoomeSpeed = 0.1f;

    public ScreenFade screenFadeMask;
    // Use this for initialization
    void Start () {
        targetPos = transform.position;
    }
	
    // Update is called once per frame
    void FixedUpdate () {
        if (target)
        {
            Vector3 posNoZ = transform.position;
            posNoZ.z = target.transform.position.z;

            Vector3 targetDirection = (target.transform.position - posNoZ);

            interpVelocity = targetDirection.magnitude * 5f;

            targetPos = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime); 
    
            Vector3 newPos = targetPos + offset;

            if (!isAtEndGame)
            {
                newPos.y = transform.position.y;

            }

            transform.position = Vector3.Lerp(transform.position, newPos, 0.25f);

        }

        if (isAtEndGame)
        {
            float camSize = GetComponent<Camera>().orthographicSize;
            if (camSize < 30)
            {
                camSize += zoomeSpeed * Time.deltaTime;
                GetComponent<Camera>().orthographicSize = camSize;
                
            }
            else
            {
                if (offset.y <= 3)
                {
                    offset.y += 1f*Time.deltaTime;

                }
                else
                {
                    if (screenFadeMask != null)
                    {
                        screenFadeMask.FadeToBlack();
                    }
                }
            }
        }
    }

    public void TriggerEndGameTransition()
    {
        //Increase camera size
        //Set end game
        isAtEndGame = true;
    }
}

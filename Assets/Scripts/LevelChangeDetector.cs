﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelChangeDetector : MonoBehaviour
{
    // Start is called before the first frame update

    public ColourShift shifter;

    public SimpleCamFollow cameraFollwo;

    public GameObject forgroundGo;

    public AudioSource endSong;

    public AudioSource startSOng;

    private bool fadeStartSong;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (fadeStartSong)
        {
            if (startSOng != null && startSOng.isPlaying)
            {
                float currentVol = startSOng.volume;
                currentVol -= 1 * Time.deltaTime;
                startSOng.volume = currentVol;
                if (currentVol <= 0.01)
                {
                    startSOng.Stop();
                }
            }
        }
    }

  

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("LevelTwo"))
        {
            shifter.TriggerLevelTwo();
            Debug.Log("TriggerLevelTwo");
        }

        if (other.gameObject.CompareTag("LevelThree"))
        {
            shifter.TriggerLevelThree();
            endSong.Play();
            fadeStartSong = true;
        }

        if (other.gameObject.CompareTag("EndGame"))
        {
            shifter.TriggerEndGame();
        }

        if (other.gameObject.CompareTag("FallingTrigger"))
        {
            cameraFollwo.TriggerEndGameTransition();
            
            Physics2D.gravity = new Vector2(0,-15);

            GetComponent<Rigidbody2D>().mass = 100;

            if (forgroundGo != null)
            {
                forgroundGo.SetActive(false);
            }
        }
        
      
    }
}

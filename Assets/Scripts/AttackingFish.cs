﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

public class AttackingFish : MonoBehaviour
{
    public GameObject crab;
    public GameObject destination;
    public float speed;

    private GameObject target;
    private Vector2 enemyVector;
    private Vector2 crabVector;
    private Vector2 destinationVector;
    private SkeletonAnimation skeletonAnimation;

    public AudioSource sound;
    private bool hasHit = false;

    public enum FacingDirection {
        UP = 270,
        DOWN = 90,
        LEFT = 180,
        RIGHT = 0
    }

    void Start(){
        target = destination;
        if(this.gameObject.name == "AnglerFish"){        
            skeletonAnimation = GetComponent<SkeletonAnimation>();
            skeletonAnimation.skeleton.SetAttachment("A-Spotlight",null);  
        }
    }

    void Update()
    {
        enemyVector = new Vector2(this.transform.position.x, this.transform.position.y);
        crabVector = new Vector2(crab.transform.position.x, crab.transform.position.y);
        destinationVector = new Vector2(destination.transform.position.x, destination.transform.position.y);

        

        float dist = Vector2.Distance(enemyVector,crabVector);
        if(dist <= 18f){
            bool isHidden = crab.GetComponent<PlayerMovement>().GetHidden();
            if(!isHidden && !hasHit){
                target = crab;
            }else{
                target = destination;
            }
            
            //Play Follow sound
            if (sound != null)
            {
                sound.Play();
            }
        }

        else
        {
            //Stop follow sound
            if (sound != null)
            {
                sound.Stop();
            }
            
        }

        float destDist = Vector2.Distance(enemyVector,destinationVector);
        if(destDist <= 1f){
            Destroy(this.gameObject);
        }

        Vector2 targetVec;
        //MoveTo();
        if(target.gameObject.name == "Sheldon" && !(hasHit) ){
            targetVec = crabVector;
        }else{
            targetVec = destinationVector;
        }
        
        MoveTo();

        if (targetVec.x > this.transform.position.x)
         {
           this.transform.localScale = new Vector3(1f,-1f,1f);
         } 
         else 
          {
              this.transform.localScale = new Vector3(1f,1f,1f);
          }
        this.transform.rotation = FaceObject(this.transform.position,targetVec, FacingDirection.LEFT);
    }   


    public static Quaternion FaceObject(Vector2 startingPosition, Vector2 crabVector, FacingDirection facing) {
        Vector2 direction = crabVector - startingPosition;
        float angle = (Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg);
        angle -= (float)facing;
        return Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private void MoveTo(){
        float step = speed * Time.deltaTime;        
            // move sprite towards the target location

            transform.position = Vector2.MoveTowards(transform.position, target.transform.position, step);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
           // other.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * 100.0f);
			target = destination;
            hasHit = true;
            
            
        }  
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourBlinker : MonoBehaviour
{
    public Color StartColor = Color.green;
    public Color EndColor;
    public float TransitionTime = 5f;
     
    private Material _myMaterial;
    // Just to make sure we don't try to lerp if we're already doing so
    private bool _transitioning = false;

    public Color colourOne;
    public Color colourTwo;
    public Color colourThree;
    public Color endColor;
    
    private void Awake()
    {
        endColor = Color.black;
        _myMaterial = GetComponent<Renderer>().material;
    }
     
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && !_transitioning)
        {
           // StartCoroutine(DoLerp(EndColor));
        }
        
        if(Input.GetKeyDown(KeyCode.T) && !_transitioning)
        {
           // StartCoroutine(DoLerp(colourOne));
        }
        
        if(Input.GetKeyDown(KeyCode.Y) && !_transitioning)
        {
            //SetLevelTwo();
        }
        
        if(Input.GetKeyDown(KeyCode.U) && !_transitioning)
        {
           // StartCoroutine(DoLerp(colourThree));
        }
    }

    public void SetLevelTwo()
    {
        StartCoroutine(DoLerp(colourOne));
    }

    public void SetLevelThree()
    {
        StartCoroutine(DoLerp(colourTwo));
    }

    public void FadeToBlack()
    {
        StartCoroutine(DoLerp(endColor));
    }
     
    private IEnumerator DoLerp(Color newCol)
    {
        _transitioning = true;
         
        float timeElapsed = 0f;
        float totalTime = TransitionTime;
         
        Color startColor = _myMaterial.color;
        Color endColor = newCol;
         
        while(timeElapsed < totalTime)
        {
            timeElapsed += Time.deltaTime;
            _myMaterial.color = Color.Lerp(startColor, endColor, timeElapsed/totalTime);
            yield return null;
        }
         
        _transitioning = false;
    }
}

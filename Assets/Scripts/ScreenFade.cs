﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenFade : MonoBehaviour
{

    private Color fullMask;

    private bool _transitioning;

    private float TransitionTime = 5f;

    private SpriteRenderer _myMaterial;
    // Start is called before the first frame update
    void Start()
    {
        fullMask = Color.white;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void FadeToBlack()
    {
        StartCoroutine(DoLerp(fullMask));
    }
     
    private IEnumerator DoLerp(Color newCol)
    {
        _transitioning = true;
         
        float timeElapsed = 0f;
        float totalTime = TransitionTime;
         
        Color startColor = GetComponent<SpriteRenderer>().color;
        Color endColor = newCol;
         
        while(timeElapsed < totalTime)
        {
            timeElapsed += Time.deltaTime;
            GetComponent<SpriteRenderer>().color= Color.Lerp(startColor, endColor, timeElapsed/totalTime);
            yield return null;
        }
         
        _transitioning = false;
    }
}


skeleton.png
size: 4096,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
Appendage-holes
  rotate: false
  xy: 3576, 1836
  size: 317, 203
  orig: 318, 203
  offset: 0, 0
  index: -1
Body
  rotate: false
  xy: 3043, 1764
  size: 531, 275
  orig: 531, 275
  offset: 0, 0
  index: -1
Can-Shell
  rotate: false
  xy: 2307, 873
  size: 664, 725
  orig: 800, 800
  offset: 81, 54
  index: -1
Can-Shell-back
  rotate: false
  xy: 2973, 925
  size: 233, 316
  orig: 394, 416
  offset: 82, 58
  index: -1
Center-leg-point
  rotate: false
  xy: 3257, 1424
  size: 147, 253
  orig: 147, 255
  offset: 0, 2
  index: -1
Center-leg-point-right
  rotate: false
  xy: 1230, 907
  size: 90, 228
  orig: 90, 228
  offset: 0, 0
  index: -1
Center-leg-upper
  rotate: false
  xy: 1344, 543
  size: 114, 219
  orig: 114, 220
  offset: 0, 1
  index: -1
Center-leg-upper-right
  rotate: false
  xy: 1230, 713
  size: 79, 192
  orig: 79, 193
  offset: 0, 0
  index: -1
Coral-shell
  rotate: false
  xy: 571, 113
  size: 391, 387
  orig: 600, 600
  offset: 95, 102
  index: -1
Large-Claw
  rotate: false
  xy: 964, 108
  size: 296, 392
  orig: 296, 393
  offset: 0, 1
  index: -1
Large-Claw-right
  rotate: false
  xy: 2973, 1243
  size: 282, 355
  orig: 282, 355
  offset: 0, 0
  index: -1
Left-eye
  rotate: false
  xy: 1460, 583
  size: 127, 179
  orig: 127, 180
  offset: 0, 1
  index: -1
Left-feeler
  rotate: false
  xy: 3043, 1679
  size: 622, 83
  orig: 622, 83
  offset: 0, 0
  index: -1
Rear-leg-point
  rotate: false
  xy: 3895, 1813
  size: 190, 226
  orig: 190, 227
  offset: 0, 0
  index: -1
Rear-leg-point-right
  rotate: false
  xy: 3257, 1212
  size: 147, 210
  orig: 147, 211
  offset: 0, 0
  index: -1
Rear-leg-upper
  rotate: false
  xy: 3208, 1005
  size: 146, 205
  orig: 146, 206
  offset: 0, 1
  index: -1
Right-eye
  rotate: false
  xy: 3406, 1202
  size: 164, 177
  orig: 165, 177
  offset: 0, 0
  index: -1
Right-feeler
  rotate: false
  xy: 2307, 714
  size: 607, 157
  orig: 607, 158
  offset: 0, 1
  index: -1
Shells-1
  rotate: false
  xy: 1334, 764
  size: 971, 834
  orig: 971, 835
  offset: 0, 1
  index: -1
Tail
  rotate: false
  xy: 2, 2
  size: 567, 498
  orig: 567, 499
  offset: 0, 0
  index: -1
Upper-claw
  rotate: false
  xy: 3406, 1381
  size: 239, 146
  orig: 240, 146
  offset: 0, 0
  index: -1
Upper-leg-rear-right
  rotate: false
  xy: 1230, 526
  size: 112, 185
  orig: 112, 186
  offset: 0, 1
  index: -1
Upper-right-claw
  rotate: false
  xy: 3406, 1529
  size: 241, 148
  orig: 241, 149
  offset: 0, 0
  index: -1
bootPart1
  rotate: false
  xy: 2, 502
  size: 1226, 633
  orig: 1228, 633
  offset: 0, 0
  index: -1
bootPart2
  rotate: false
  xy: 2, 1137
  size: 1330, 902
  orig: 1330, 902
  offset: 0, 0
  index: -1
bootPart3
  rotate: false
  xy: 1334, 1600
  size: 1707, 439
  orig: 1707, 439
  offset: 0, 0
  index: -1
